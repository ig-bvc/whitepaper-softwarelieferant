# Whitepaper für Softwarelieferanten
Dieses Repository enthält die Quellen des Whitepaper für Softwarelieferanten als Markdown. Die automatisierte Erstellung ist mit [Pandoc](https://pandoc.org/) möglich.

## Dokumentation lokal erzeugen
Für die lokale Erzeugung unter Linux ist das convenience Skript ```generate.sh``` vorhanden

     $ ./generate.sh

Das Skript benötigt die Container Runtime Docker , um die Generierung mittels Pandoc im Container auszuführen.

Das Whitepaper wird als PDF erstellt und ist bei erfolgreicher Generierung in der Datei ```whitepaper.pdf``` zu finden.

Arbeitsweise Gitlab-Tickets: 

1. Issue wird erstellt. Von uns oder von einem externen Reporter.
2. Hinweise
   1. Wir besprechen in der Arbeitssitzung, ob wir das Issue akzeptieren und welche Arbeiten dafür zu tun sind.
   2. Wir entscheiden, dass das Issue nicht bearbeitet wird und schließen das Issue. Dabei wird in einem Kommentar die Begründung festgehalten und dass keine Bearbeitung erfolgt ist.
3. wir erzeugen einen Branch aus dem Issue (noch keinen Merge Request). Den Branchnamen prefixen wir mir bugfix/ oder feature/, nach git-Konvention
4. Die Arbeiten werden asynchron erledigt und an gitlab gepusht.
5. Sind alle Arbeiten abgeschlossen, wird ein MergeRequest erstellt. In der Beschreibung wird mit "Closes Issue: #<Issue-Nummer>" der Issue referenziert.
6. Es wird ein Reviewer festgelegt, der ein Review durchführt. Bzw. wir diskutieren das im Arbeitstermin. Solange der MergeRequest auf Draft steht, muss der Reviewer nicht tätig werden.
7. Erst nach erfoglreichem Approval durch den Reviewer und Bauen kann gemerged werden. (Auch wenn wir das technisch nicht durch Approval-Rules enforcen können.)
8. Sind alle Arbeiten für den Issue damit erledigt, wird der Issue geschlossen. Dabei wird in einem Kommentar festgehalten, dass eine Bearbeitung stattgefunden hat.