#!/bin/bash

docker run --rm -it --volume "$(pwd):/data" pandoc/extra:3 -d defaults.yaml -o whitepaper.pdf $(tr '[:space:]' ' ' < includes.txt)
