### Images

Der Softwarelieferant MUSS...

* sicherstellen, dass gelieferte Artefakte, insb. Images, neben den in diesem Dokument genannten Anforderung, auch weiteren relevanten IT-Grundschutz-Bausteinen genügen.[^Images0] (SYS.1.6.A10 S)
* das Image mit Metainformation versehen und signieren.[^Images1] (SYS.1.6.A12 S)
* bewerten und sicherstellen, dass sämtliche Bestandteile seiner Anwendungsimages aus vertrauenswürdigen Quellen stammen. Dabei kann auf vom Plattformbetreiber freigegebene Base-Images aufgesetzt werden. (SYS.1.6.A6 S)
* dem Softwarebetreiber Container Images mit allen zur Runtime benötigten Modulen bereitstellen. Ein Nachladen von Modulen DARF NICHT erfolgen. (SYS.1.6.A6 S)
* regelmäßig Images auf Schwachstellen und Schadcode prüfen[^Images3] und bei Bedarf geeignete Updates zur Verfügung stellen.[^Images4] (SYS.1.6.A6 S)


Der Softwarebetreiber MUSS...

* mit dem Softwarelieferanten die Basis für gelieferte Images abstimmen. (SYS.1.6.A9 S)
* Vorgaben für möglichst minimale Images festlegen und veröffentlichen. (SYS1.6.A6 S)

Der Plattformbetreiber MUSS ...

* Richtlinien für die Bereitstellung von Images dem Softwarelieferanten zur Verfügung stellen (SYS1.6.A6 S)
* Images unmittelbar bei der Bereitstellung in geeigneter Weise auf Schadcode und Schwachstellen prüfen. (SYS1.6.A6 S)

Der Plattformbetreiber SOLL ...

* automatisierte Policies implementieren, die die Herkunft, Vertrauenswürdigkeit und Integrität der Images prüfen und durchsetzen. (SYS1.6.A6 S)

[^Images0]: Neben den ITGS-Bausteinen SYS.1.6 und APP.4.4 sind für die Container-Lösungen ggf. weitere ITGS-Bausteine wie CON.8 "Software-Entwicklung" und APP.3.1 "Webanwendungen und Webservices" zu beachten.

[^Images1]: Es muss eine Liste von Metadaten die mindestens vorhanden sein müssen existieren (Versionierung und Tagging von Images).

[^Images3]: Gängig sind in diesem Zusammenhang Security Scanner der Image Registry.

[^Images4]: Welche Reaktionszeiten angemessen sind, ist vertraglich zu definieren.
