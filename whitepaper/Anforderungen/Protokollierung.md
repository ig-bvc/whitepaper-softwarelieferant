## Protokollierung

Der Softwarelieferant MUSS...

* alle Protokolldaten der Anwendung im Container über die Standardausgabe ausgeben.[^Log1] (SYS.1.6.A7 B)

[^Log1]: Log-Daten MÜSSEN über STDOUT / STDERR ausgegeben werden. 
