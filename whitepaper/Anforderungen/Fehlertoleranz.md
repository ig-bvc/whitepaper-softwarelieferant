## Fehlertoleranz

Der Softwarelieferant MUSS...

* eine Fehlertoleranz für die Anwendung für den Wiederanlauf nach einem ungeordneten Abbruch der Datenverarbeitung sicherstellen.[^Error1] (APP.4.4.A19 H)

Der Softwarelieferant SOLL...

* die Anwendung Stateless gestalten, um die Mittel der Plattform bei einer Umsetzung von Hochverfügbarkeit nutzen zu können.[^Error2] (APP.4.4.A19 H)
* Container stateless gestalten und eine transaktionsoriente Funktionsweise der Anwendung sicherstellen.[^Error3] (SYS.1.6.A9 S)

Der Softwarelieferant KANN...

* Start-up-Checks für den Start der Container implementieren.[^Error5] (APP.4.4.A11 S)


[^Error1]: Mögliche Maßnahmen: atomare Gestaltung, keine persistente Daten im Container. Synchrone Transaktionen falls mehrere Datenbanken, Webservices usw. genutzt werden. Mechanismus zur Prüfung der Konsistenz der Daten, ggf. Mittel zur Wiederherstellung der Konsistenz.

[^Error2]: Die Optionen hinsichtlich Zustandslosigkeit sind für Bestandsanwendungen beschränkt und lassen sich ggf. nicht umsetzen.

[^Error3]: Die transaktionsorientierte Funktionsweise soll sicherstellen, dass alle Verarbeitungen korrekt abgeschlossen werden können und keine inkonsistenten Zustände entstehen, selbst wenn Container neu deployed werden.
  
[^Error5]: Siehe https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-startup-probes
