## Update- und Patchmanagement

Der Softwarelieferant MUSS...

* ein Patchmanagement gewährleisten. (SYS.1.6.A10 S)
* sicherstellen, dass Updates von Software nicht im laufenden Container installiert werden. Bei persistenten Containern SOLLTE geprüft werden, ob in (absolut seltenen) Ausnahmefällen ein Update des jeweiligen Containers geeigneter ist, als den Container vollständig neu zu provisionieren.[^Update1] (SYS.1.6.A14 S)
* bei sicherheitsrelevanten und featurebasierten Updates der Anwendungen oder der Standardimages neue Images erstellen und eindeutig versioniert innerhalb der vereinbarten Zeiträume / SLAs zur Verfügung stellen. (SYS.1.6.A14 S)
* das Rechte- und Rollenkonzept für die Verwaltung des Sourcecodes umsetzen.[^Update3] (APP.4.4.A2 B) 

Der Softwarelieferant SOLL...

* sicherstellen, dass die Software in der Lage ist, den Service auch während Updates entsprechend der definierten Anforderungen aufrechtzuerhalten.[^Update2] (SYS.1.6.A14 S)

Der Softwarebetreiber MUSS...

* sicherstellen, dass die Software neu angelieferter Images auf Funktionionalität und Schadcode geprüft und innerhalb der vereinbarten Zeitfenster / SLAs für den produktiven Container-Betrieb bereitgestellt werden. (SYS.1.6.A14 S) 

Der Softwarebetreiber SOLL...

* die benötigten SLAs zur Aktualisierung der Images mit dem Softwarelieferanten vereinbaren. (SYS.1.6.A14 S)
* sicherstellen, dass keine veralteten Images zum Einsatz kommen und sicher stellen, dass neue Images regelmäßig vom Softwarelieferanten in die eigene Registry übernommen werden. (SYS.1.6.A14 S)
* gemäß OPS.1.1.3 Patch- und Änderungsmanagement entscheiden, wann und wie die Updates der Images oder der betriebenen Software bzw. des betriebenen Dienstes ausgerollt werden. (SYS.1.6.A14 S)

[^Update1]: Es dürfen keine Tools enthalten sein, welche nicht für den Betrieb der Applikation notwendig sind (Paketmanager, YUM, APT, APK usw.).

[^Update2]: d.h. Clusterfähigkeit der Software

[^Update3]: siehe auch CON.8

