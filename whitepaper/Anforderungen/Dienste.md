### Dienste und Service-Accounts

Der Softwarelieferant MUSS...

* bei Neuentwicklungen die Software so gestalten, dass jeder Container nur einen Dienst bereitstellt. (SYS.1.6.A11 S)
* für Dienste, die unterschiedlich skalieren oder auf verschiedenen Worker-Nodes betrieben werden sollen, entsprechende Parameter der Deployment Descriptoren vorsehen.[^Dienste1] (SYS.1.6.A11 S)
* pro Anwendung (mindestens) einen eigenen ServiceAccount bereitstellen, mit dem die Anwendung vollständig betrieben werden kann. (APP.4.4.A9 S) 
* die Software so entwickeln, dass die Service-Accounts mit den geringst möglichen Berechtigungen (Least-Privilege) betrieben werden können. (APP.4.4.A9 S)

Der Softwarelieferant SOLL...

* bei der Nutzung von zeitlich wiederkehrenden Aktivitäten auf Systemebene (z.B. CRON-Jobs) die Mechanismen der Container-Plattform zur Ausführung nutzen. (SYS.1.6.A9 S)
* die Container so auslegen, dass eine horizontale Skalierung möglich ist. (SYS.1.6.A9 S)
* die Software so gestalten, dass jeder Container nur einen Dienst bereitstellt.[^Dienste3] (SYS.1.6.A11 S)
* die Software so gestalten, dass Pods nicht den default-Service-Account benötigen.[^Dienste4] (APP.4.4.A9 S) 

[^Dienste1]: Für Helm Charts ist die Parametrisierung über die values.yaml vorzusehen.

[^Dienste3]: Die Einstufung mit "soll" ist nur aufgrund der Übergangsphase aus der "klassischen" in die Containerwelt vorgenommen worden. Es wird ein "muss" angestrebt.
 
[^Dienste4]: Bei OpenShift sind die Rechte des "default" Service Account bereits maximal eingeschränkt.
