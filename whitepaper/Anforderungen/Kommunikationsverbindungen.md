### Kommunikationsverbindungen

Der Softwarelieferant MUSS...

- die Software so bereitstellen, dass keine Fernwartung möglich ist.[^Komm1] (SYS.1.6.A16 S)
- Zugriffe und Berechtigungen auf Ressourcen durch seine Software auf die technisch notwendigen Zugriffe beschränken. (APP.4.4.A21 H und SYS.1.6.A21 H)
- die Vorgaben und Rahmenbedingungen des Software- und des Plattformbetreibers für die Kommunikationsbeziehungen berücksichtigen und seine Implementierung entsprechend auslegen.[^Komm3] (SYS.1.6.A5 B)
- die eingehende und ausgehende Kommunikation der Anwendung über Kubernetes-Serviceressourcen (Ingress und Egress) umsetzen.
- die notwendigen Kommunikationsbeziehungen (auch innerhalb von Namespaces) zwischen den Komponenten der Anwendung (z.B. Containern und Pods), sowie mit Komponenten außerhalb der Container-Plattform in geeigneter Weise dokumentieren. (APP.4.4.A18)

Der Softwarelieferant SOLL...

- die zertifikatsbasierte Authentifizierung unterstützen und Kommunikationsverbindungen nur für die in den Zertifikaten hinterlegten Identitäten erlauben.[^Komm4] (APP.4.4.A18)

[^Komm1]: Die Container Images DÜRFEN KEINE Komponenten zur Fernwartung (zum Beispiel ssh, telnet) enthalten.

[^Komm3]: Bei Anbietern für Standardsoftware sollte der Plattformbetreiber prüfen, ob seine Anforderungen dem Marktangebot gerecht werden.

[^Komm4]: z.B. mittels mTLS. Es sollte mindestens eine Serverauthentifizierung umgesetzt werden. Eine Client-Authentifizierung sollte möglich werden. Die Umsetzung kann auch über einen Service-Mesh (Bereitstellung durch Plattformbetreiber notwendig) umgesetzt werden.
