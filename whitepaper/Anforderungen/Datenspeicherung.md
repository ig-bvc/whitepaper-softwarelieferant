### Datenspeicherung

Der Softwarelieferant MUSS...

* die Nutzung von temporärem Speicher so vorsehen, dass ein Restart des Containers und ein Wechsel der Nodes zur Laufzeit möglich ist. (SYS.1.6.A9 S)
* notwendige persistente Volumen und deren Nutzung (z. B. RWM, RWO, RW/RO) im Deployment Descriptor beschreiben.[^Data1] (SYS.1.6.A19 S)
* sicherstellen, dass keine Zugangsdaten (z.B. Passworte, geheime/private Schlüssel, API-Keys, Schlüssel für symmetrische Verschlüsselungen) in Container-Images bzw. in Konfigurationsdateien gespeichert werden.[^Data5] (SYS.1.6.A8 B) 
* die Software so gestalten, dass diese keine Geheimnisse und Kennwörter als Plain-Text nutzt. (APP.4.4.A2 B)
* für seine Software sicherstellen, dass diese mit minimalen Berechtigungen lauffähig ist.[^Data6] (SYS.1.6.A21 H)
* die Container mit Ressourcenbeschränkungen ausliefern (z.B. Datenmenge und Speicherzugriffe). (SYS.1.6A23 H)
* Daten in eigene Mount-Verzeichnisse schreiben. (SYS.1.6A23 H)
* sicherstellen, dass Daten, die im Container ausschließlich gelesen werden, als Read-Only-Volume in den Container eingebunden werden. (SYS.1.6A23 H)

Der Softwarelieferant SOLL...

* keine lokalen Speicher der Workernodes benutzen.[^Data4] (SYS.1.6.A19 S)
* für seine Software sicherstellen, dass diese nicht in das Root-File-System des Containers schreiben darf. (SYS.1.6A23 H)
* das temporäre Speichern von Daten[^Data3] in dediziert dafür bereitgestellten Volumes (gängigerweise Ephemeral Storage) vornehmen. (SYS.1.6.A23 H)


[^Data1]: z.B. Zielpfad im Containerbetrieb, Größe, Berechtigungen, Dateisystem, Performance. Die Beschreibung sollte in einer formalen Sprache erfolgen. Das Physical Volume Claim soll dynamisch erfolgen. Bitte mögliche Einschränkungen im Rechenzentrum beachten.

[^Data3]: bspw. Daten in temp-Verzeichnissen, wie File-Uploads, etc.

[^Data4]: Es sollte nur persistent Storage genutzt werden. "Sollte" bietet Ausnahmen für flüchtige Anwendungsfälle \[virenscan\].

[^Data5]: Anstatt statische Secrets auszuliefern können Secrets beim Deployment generiert werden.

[^Data6]: Beispielsweise sollen für Anwendungen, die nur Lesezugriff benötigen, nur Nutzer verwendet werden, die keinen Schreibzugriff haben.
