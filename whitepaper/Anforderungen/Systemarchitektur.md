### Systemarchitektur

Der Softwarelieferant MUSS...

* Software-Produkte so entwickeln, dass ein Re-Deployment auf einem "Restore-Cluster" jederzeit möglich ist (APP.4.4.A5 B)

Der Softwarelieferant SOLL...

* die Anwendung so bereitstellen, dass die lt. Schutzbedarf erforderliche Anwendung von technischen Schutzmaßnahmen wie z.B. die Verwendung von Betriebssystem-Mechanismen, die Isolation und Kapselung der Anwendung und auch die Netzwerktrennung durch die Software-Architektur unterstützt und nicht verhindert wird.[^Arch1] (SYS.1.6.A3 B, APP.4.4.A4 B)
* Software-Produkte so entwickeln, dass zu jedem beliebigen Zeitpunkt eine konsistente Datensicherung seiner Anwendung durch den Software- oder Plattformbetreiber durchgeführt werden kann und eine Point in Time Wiederherstellung möglich ist.[^Arch2] (APP.4.4.A5 B)


Der Plattformbetreiber MUSS ...

* sicherstellen, dass der Betriebssystemkernel der Nodes Isolationsmechanismen unterstützt. Es muss eine Trennung auf Prozess-ID, Inter-Prozesskommunikation, Benutzer-ID, Dateisystem und Netzwerk möglich sein. Die genutzten Isolationsmechanismen müssen dokumentiert sein und dem Softwarelieferanten und dem Softwarebetreiber bei Bedarf zur Verfügung gestellt werden. (APP.4.4.A4 B)
* der Plattformbetreiber MUSS technische Maßnahmen anbieten, die eine Isolation und Kapselung containerisierter IT-Systeme sowie die Trennung dieser IT-Systeme in unterschiedliche virtuelle Netze ermöglichen. Dabei muss die Netzwerkarchitektur (physische, virtuelle und Overlay-Netze) so gestaltet werden, dass die Anforderungen an einen sicheren Netzwerkbetrieb lt. BSI-Grundschutz NET.1 und NET.3 erfüllt sind. (SYS.1.6.A3 B)


[^Arch1]: durch Plattform vorgegebene Isolationsmechanismen wie z.B. SE Linux und CGroups; plattformspezifische Isolationsmechanismen wie z.B. Namespaces, Pod Security Admission und Network Policies

[^Arch2]: Bei einem Recovery ist der exakt gleiche Zustand des Clusters (gleiche Anzahl an Pods) nicht erreichbar. Folgende Mechanismen sollten beachtet werden: Festspeicher (Persistent Volumes); Konfigurationsdateien von Kubernetes und den weiteren Programmen der Control Plane; plattformspezifische Erweiterungen wie z.B. Monitoring, Logging, Ingress etc.; Datenbanken der Konfiguration, namentlich hier etcd; alle Infrastrukturanwendungen die zum Betrieb des Clusters und der darin befindlichen Dienste notwendig sind; die Datenhaltung der Code und Image Registries
