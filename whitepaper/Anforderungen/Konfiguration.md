## Konfiguration/Administration

Der Softwarelieferant MUSS...

* Konfigurationsanpassungen so ausführen, dass keine manuellen Anpassungen in laufenden Containern erfolgen. (SYS.1.6.A9 S)
* die Software so gestalten, dass die Container-Runtime und alle instanziierten Container nur von einem nicht-privilegierten System-Account ausgeführt werden, der keine erweiterten Rechte für den Container-Dienst bzw. das Betriebssystem des Host-Systems benötigt oder diese Rechte erlangen kann. 
Ausnahmen hiervon sind Container, welche Aufgaben des Host-Systems übernehmen. [^Konfig1] (SYS.1.6.A17 S)
* die Privilegien für Container auf das erforderliche Minimum begrenzen. (SYS.1.6.A17 S)	
* die Informationen zur Einschränkung der Nutzung von erweiterten Privilegien dem Softwarelieferanten zur Verfügung stellen. (SYS.1.6.A17 S)
* die Software so gestalten, dass die genutzten User- und Group-ID's keine  Berechtigungen auf die System- und Datenbereiche des Hosts erfordern. (SYS.1.6.A18 S)
* die erforderlichen User- und Group-ID's und deren Berechtigungen benennen (z.B. in den Konfigurationsdateien). (SYS.1.6.A18 S)
* die mitgelieferte Konfiguration der Container versioniert bereitstellen und den Bezug zu Imageversionen sicherstellen. [^Konfig2] (SYS.1.6.A20 S)
* das vom Softwarebetreiber benötigte Verteilungsschema für die Pods unterstützen. [^Konfig5] (APP.4.4.A14 H)
* notwendige Berechtigungen im Deployment beschreiben. Diese müssen minimal gewählt werden. (APP.1.6.A19 S)
* den Abschluss eines Init-Container sicherstellen. (APP.4.4.A6 S)
* den Init-Container eindeutig von der Applikation logisch trennen. (APP.4.4.A6 S)

Der Softwarelieferant SOLL...

* das automatische Management seiner Software durch die Bereitstellung von geeigneten Administrationswerkzeugen unterstützen.[^Konfig3] (SYS.1.6.A2 B)
* Init-Container für Konfigurationsanpassungen nutzen. (SYS.1.6.A9 S)
* die Deployments entsprechend der Richtlinien des Softwarebetreibers ausrichten.[^Konfig4] (APP.4.4.A13 H)
* bei besonders kritischen Anwendungen Operatoren zur Automatisierung von Betriebsaufgaben liefern. (APP.4.4.A16 H)


Der Softwarebetreiber MUSS...

* dem Softwarelieferanten geeignete Möglichkeiten zum Geheimnisaustausch bieten. (SYS.1.6.A20 S)  

Der Plattformbetreiber MUSS...

* geeignete Möglichkeiten zum mandantenfähigen Geheimnismanagement bereitstellen. (SYS.1.6.A20 S) 

[^Konfig1]: kein "privileged"-Mode für die Container-Runtime und den laufenden Containern
Beispiel für Ausnahmen sind: ingress, egress und Infrastrukturcontainer.
Beispiele für Aufgaben des Hostsystems sind: i) ingress und egress sind Bastion-Node-Aufgaben, ii) Bereitstellung von Speichersystemen sind Speicher-Node-Aufgaben

[^Konfig2]: Entweder z.B. durch Label in der YAML oder als git-repo.

[^Konfig3]: Werkzeuge können Deployment-Scripte, Pipeline-Scripte für CI/CD, Kubernetes Operatoren sein.

[^Konfig4]: Softwarebetreiber muss Richtlinien für die Einstellungen von Deployments definieren und dem Softwarelieferanten bekannt geben.

[^Konfig5]: z.B. Aufteilung der Pods entsprechend der Aufgaben. Der Softwarebetreiber muss das Verteilungsschema für die Pods definieren. Die Nodes müssen mindestens nach folgenden Aufgaben unterschieden und getrennt werden:
   i) Bastion-Nodes zur Realisierung von ingress und egress, ii) Anwendungs-Nodes zum Betrieb der Pods für die Anwendungen, iii) Speicher-Nodes zur Bereitstellung der Speicherlösungen, iv) Management-Nodes für den Cluster
