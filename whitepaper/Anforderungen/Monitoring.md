### Monitoring

Der Softwarelieferant SOLL...

* Liveness- und Readiness-Checks gemäß der Kubernetes-Dokumentation[^Monitoring1] bereitstellen. Die Checks, insbes. die Readiness-Checks, sind anwendungsspezifisch zu betrachten.[^Monitoring2] (APP.4.4.A11 S)
* die forensische Analyse unterstützen, indem u.a. die eingehenden und ausgehenden Requests, sowie  Konfigurationsänderungen geloggt werden. (SYS.1.6.A22 H)

Der Softwarelieferant KANN...

* Start-up-Checks für den Start der Container implementieren.[^Monitoring4] (APP.4.4.A11 S)

[^Monitoring1]: siehe https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/

[^Monitoring2]: siehe einschlägige Best-Practices, bspw. https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-best-practices-setting-up-health-checks-with-readiness-and-liveness-probes

[^Monitoring4]: Hinweis: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-startup-probes
