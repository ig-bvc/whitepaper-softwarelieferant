# Grundsätze für die Entwicklung von Containerlösungen

Die IG Betrieb von Containern (IG BvC) ist ein Zusammenschluss von
Datenzentralen, Softwarelieferanten und Organisationen der Öffentlichen
Verwaltung.

Ziel der IG BvC ist die Schaffung von Standards für das Deployment von
Fachverfahren in Container-Umgebungen sowie für den Betrieb von
Container-Plattformen zur Herstellung einer Kompatibilität zwischen den
Rechenzentren. Die Schaffung einer herstellerunabhängigen Systematik ist
das Grundverständnis der IG BvC.

Die Schaffung und Umsetzung von konkreten Möglichkeiten der Koppelung
und gemeinsamen Nutzung von Container-Plattformen zwischen den
Rechenzentren von Bund, Ländern und Kommunen ist ein wesentlicher
Grundsatz für die Zusammenarbeit.

Die Ausarbeitungen der AG Cloud und Digitale Souveränität werden bei der
Ausarbeitung der Standards genutzt. Im Gegenzug werden die
Ausarbeitungen der IG BvC für die Nutzung durch die AG bereitgestellt.

Als grundlegender Standard für die
Betrachtungen wird Kubernetes gesetzt. Sämtliche Betrachtungen werden
plattformunabhängig ausgeführt um eine Abhängigkeit von Produkten und
Herstellern weitestgehend auszuschließen.

Die folgenden beiden Bausteine des BSI-Grundschutzes (Edition 2023) bilden die Grundlage

* SYS.1.6 Containerisierung
* APP.4.4 Kubernetes

Gängigerweise sind weitere IT-Grundschutz-Bausteine zu beachten. Diese sind hier zu finden:

[https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung
/IT-Grundschutz/IT-Grundschutz-Kompendium/IT-Grundschutz-Bausteine/Bausteine_Download_Edition_node.html](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung
/IT-Grundschutz/IT-Grundschutz-Kompendium/IT-Grundschutz-Bausteine/Bausteine_Download_Edition_node.html")
