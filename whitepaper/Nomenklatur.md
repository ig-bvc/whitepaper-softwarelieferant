# Nomenklatur

Die Verwendung und Bedeutung der modalen Hilfsverben (alternativ
Modalverben) ist in der DIN-Norm 820-2 oder RFC2119 geregelt. Ergänzend
zu diesen Regeln werden die Modalverben bzw. Schlüsselwörter „MUSS",
„SOLL", „IST ZU", „DARF NICHT", „SOLLTE", „SOLLTE NICHT", „KANN" und
„DARF" wie folgt verwendet:

-   „**MUSS**", „**IST ZU**", „**DARF NUR**" weisen auf eine absolut zu
    erfüllende Anforderung hin (uneingeschränkte Anforderung).

-   „**DARF NICHT**", „**DARF KEIN**" ist die Negierung einer
    „MUSS"-Anforderung und stellt ein Verbot dar (uneingeschränktes
    Verbot).

-   „**SOLLTE**", „**SOLL**" oder das entsprechende Adjektiv „EMPFOHLEN"
    bedeuten, dass eine Anforderung normalerweise erfüllt werden muss,
    es aber Gründe geben kann, dies doch nicht zu tun. Dies muss aber
    sorgfältig abgewogen und stichhaltig begründet werden.

-   „**SOLLTE NICHT**" oder das korrespondierende Adjektiv „NICHT
    EMPFOHLEN" sind die entsprechenden Negierungen und bedeuten, dass
    etwas normalerweise nicht getan werden sollte, es aber Gründe
    gibt, dies doch zu tun. Dies muss aber sorgfältig abgewogen und
    stichhaltig begründet werden.

-   „**KANN**", „**DARF**" zeigt eine Option an.

Folgende Definitionen für Rollen werden in der IG Betrieb von Containern verwendet:

-   Der **Softwarelieferant** ist eine Organisation (juristische Person,
    Community), welche Softwarereleases bereitstellt.\
    Wenn möglich setzt sie die Anforderungen des Software- und
    Plattformbetreibers um.

-   Der **Plattformbetreiber** betreibt die IT-Infrastruktur Kontext
    IaaS und PaaS für die Containerumgebung im Rechenzentrum und stellt
    Mittel zur manuellen und/oder automatischen Orchestrierung bereit.

-   Der **Softwarebetreiber** verantwortet den Betrieb einer Anwendung /
    eines Services entsprechend vertraglicher Verpflichtungen gegenüber
    Kunde/Auftraggeber und managed die Orchestrierung von Containern.\
    Wenn möglich, stimmt er die Anforderungen an den Betrieb der
    Software mit dem Softwarelieferanten ab.\
    Es ist das Bindeglied zwischen Plattformbetreiber und
    Softwarelieferant.

