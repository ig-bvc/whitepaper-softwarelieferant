
## Versionshistorie
| Version | Datum | Status, Änderungsgrund |
| -- | --- | ----------- |
| 1.0    | 17.06.2021 | Erstversion |
| 2.0    | 15.06.2023 | Anpassung auf BSI Grundschutz 2022, Bausteine APP4.4 und SYS1.6 |
| 2.1    | 06.05.2024 | CRs der AG Blickwinkel Softwarelieferant |
