## Lizenzen

Der Softwarelieferant MUSS...

* eine Dokumentation bereitstellen, aus der hervorgeht, welche Lizenzen für den Betrieb der Software auf der Container-Plattform erforderlich sind[^DocLizenz1]. (SYS.1.6.A9 S)

[^DocLizenz1]: Die Bereitstellung von Lizenzen hängt von den jeweiligen vertraglichen Vereinbarungen ab.
