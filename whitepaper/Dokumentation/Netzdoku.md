## Kommunikationsbeziehungen dokumentieren

Der Softwarelieferant MUSS...

* alle erforderlichen Kommunikationsverbindungen inklusive Ziele, Kommunikationsprotokolle und Ports beschreiben, die für Inbetriebnahme und Betrieb erforderlich sind. (APP.4.4.A7 S [^DocKomm3], siehe auch SYS.1.6.A5 B, siehe auch APP.4.4.A14 H [^DocKomm2]
).
* benötigte Zugriffe und Berechtigungen auf Ressourcen dokumentieren. (APP.4.4.A21 H)
* dem Softwarebetreiber die von Ihm definierten erweiterten Maßnahmen mitteilen, sofern vorhanden. [^DocKomm1] (SYS.1.6.A26 H) 

Der Softwarelieferant SOLL...

*  Dimensionierungen (Requests) und Begrenzungen (Limits) auf den von ihm angenommenen Nutzerprofil für das Netzwerk definieren und dem Softwarebetreiber mitteilen. (SYS.1.6.A15 S)

Der Softwarelieferant KANN...

* in Abstimmung mit dem Softwarebetreiber die Kommunikationsbeziehungen in einer technisch formalen Form darstellen. (APP.4.4.A7 S [^DocKomm4], siehe auch SYS.1.6.A5 B)

 
[^DocKomm1]: Feste Zuordnung von Containern zu Container-Hosts, Ausführung der einzelnen Container und/oder des Container-Hosts mit Hypervisoren und feste Zuordnung eines einzelnen Containers zu einem einzelnen Container-Host.

[^DocKomm2]: Auch die Kommunikation innerhalb der Anwendung muss bekannt und beschrieben sein.

[^DocKomm3]: Bei klar definierten Zielen sollten FQDN oder IP-Adressen zur Beschreibung genutzt werden.

[^DocKomm4]: siehe https://kubernetes.io/docs/concepts/services-networking/network-policies
