## Dokumentation von Anforderungen an persistenten Speicher

Der Softwarelieferant MUSS...

* die Dimensionierungen und die Größe des benötigten persistenten Speichers empfehlen. Die Informationen müssen dem Softwarebetreiber mitgeteilt werden. [^DocStorage1] (SYS.1.6.A15 S)


[^DocStorage1]: Der Softwarelieferant muss Defaultwerte für die Limits liefern, die dann ggf. vom Softwarebetreiber angepasst werden können.

