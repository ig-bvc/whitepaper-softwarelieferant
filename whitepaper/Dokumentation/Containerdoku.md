## Dokumentation des Containers

Der Softwarelieferant MUSS...

* dokumentieren, welche Quellen für Images verwendet wurden. (SYS.1.6.A12 S)
* beschreiben, welche Quellen für Bestandteile des Image verwendet wurden, inklusive eines ggf. genutzten Basis-Images[^DocImage1] (SYS.1.6.A12 S)
* Dimensionierungen (Requests) und Begrenzungen (Limits) für CPU, RAM sowie temporären Speicher für jeden Container auf den von ihm angenommenen Nutzerprofil empfehlen.[^DocImage2] (SYS.1.6.A15 S)
* die Möglichkeiten und Funktionsweisen zur Skalierung der Dienste beschreiben.[^DocImage3] (SYS.1.6.A15 S)
* Konfigurationsoptionen für die Protokollierung beschreiben.[^DocImage5] (SYS.1.6.A7 B)
* den Einsatz eines Init-Containers dokumentieren.[^DocImage6] (APP.4.4.A6 S) 
* die Ressourcenanforderungen an die Init-Container beschreiben und dem Softwarebetreiber mitteilen. (APP.4.4.A6 S)

Der Softwarelieferant SOLL...

* ein für seine Software erwartbares Verhalten (Systemaufrufe, Kommunikationsbeziehungen usw.) dokumentieren.[^DocImagesS1] (SYS.1.6.A24 H)
* die Umsetzung der Richtlinien und Vorgaben mit dem Softwarelieferanten vertraglich festhalten.[^DocImage4] (SYS.1.6.A10 S)
* Handlungsempfehlungen bereitstellen, für den Fall,
 dass ein Container an seine Ressourcengrenzen kommt. (SYS.1.6.A15 S)
* die Möglichkeiten der Hochverfügbarkeit darstellen (SYS.1.6.A25 H)

Der Softwarelieferant KANN...

* dem Softwarebetreiber ein Klassifikationssystem für die Erstellung einer Whitelist für vertrauenswürdige Quellen vorschlagen. (SYS.1.6.A12 S)


Der Softwarebetreiber MUSS...

* die Richtlinien und Vorgaben zum sicheren Betrieb dem Softwarelieferanten bereitstellen. (SYS.1.6.A10 S)
* die Klassifikation von vertrauenswürdigen Quellen vornehmen und dem Softwarelieferant mitteilen. (SYS.1.6.A12 S)
* die Klassifizierung der vertrauenswürdigen Quellen begründen. (SYS.1.6.A12 S)
* die angegebenen Begrenzungen der Ressourcen (Limits) durch entsprechende Konfiguration der Container umsetzen. (SYS.1.6.A15 S)

[^DocImage1]: Dies dient der Verdeutlichung des konzeptionellen Aufbaus der Software (z.B. zur Beurteilung hinsichtlich der verwendeten Lizenzen).

[^DocImage2]: Der Softwarelieferant muss Defaultwerte für die Limits liefern, die dann ggf. vom Softwarebetreiber angepasst werden können.

[^DocImage3]: z.B. kubernetes pod hpa - https://kubernetes.io/de/docs/tasks/run-application/horizontal-pod-autoscale/ - Anmerkung: Limitierung muss auch berücksichtigt werden, wenn der Dienst skaliert werden soll.

[^DocImagesS1]: Wenn der Softwarelieferant das normale Verhalten &mdash; wie oben gefordert &mdash; dokumentiert, dann kann der Softwarebetreiber Abweichungen feststellen und auf etwaige Sicherheitvorfälle reagieren.

[^DocImage4]: Insbesondere bei der Lieferung von Images, da hier die Fähigkeit der Anpassung der Images beschränkt wird.

[^DocImage5]: Der Softwarebetreiber soll in die Lage versetzt werden, die Protokollierung , bspw. Log-Level, -Format oder -Kategorien, einzurichten.

[^DocImage6]: Es müssen die Berechtigungen dargestellt werden. Er muss die notwendigen Ressourcen dokumentieren und eine Empfehlung bereitstellen.
