## Berechtigungen

Der Softwarelieferant MUSS...

- darlegen, für welche Zwecke technische User (z.B.: Kubernetes ServiceAccounts) Berechtigungen erfordern[^DocBerech1] [^DocBerech2]  (APP.4.4.A3 B) 


[^DocBerech1]: Der Softwarebetreiber muss die benötigten Accounts und Berechtigungen beim Plattformbetreiber anfordern und sie müssen den Privilegienrichtlinien entsprechen

[^DocBerech2]: Der Softwarebetreiber muss anonyme Zugriffe für administrative Handlungen verhindern. 
