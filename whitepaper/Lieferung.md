# Lieferung

Der Softwarelieferant MUSS...

* eine vollständige Software Bill of Materials (SBOM)[^Liefer1] in einem Format entsprechend TR-03183[^Liefer4] bereitstellen (SYS.1.6.A13 S)
* einen definierten Prozess für den Bezug und die Weitergabe von Images etablieren und nachvollziehbar dokumentieren.[^Liefer2] (SYS.1.6.A4 B)
* einen standardkonformen Übergabepunkt zum Softwarebetreiber verwenden.[^Liefer3] (SYS.1.6.A4 B)
* die unterschiedlichen Schutzbedarfe bei der Bereitstellung der Lösung unterstützen.[^Liefer7] (APP.4.4.A2 B)

Der Softwarelieferant SOLL...

* Deployment Manifeste zur Installation bereitstellen. (SYS.1.6.A9 S)
* sicherstellen, dass zusammenhängende Dienste durch das Deployment bzw. die Orchestrierung als Verwaltungseinheiten bereitgestellt werden können und z.B. geeignete Deployment-Artefakte (Manifeste) liefern. (SYS.1.6.A11 S)
* für die durch ihn bereitgestellten Software-Artefakte automatisiert zu verarbeitende Deployment-Anweisungen bereitstellen (z.B. YAML-basierte Deployments). (SYS.1.6.A11 S)
* neben einem Freigabeprozess für die erstellte Software[^Liefer5] auch einen Freigabeprozess für Images und Deployment Deskriptoren umsetzen. (SYS.1.6.A13 S)
* die Freigabebedingungen in einem definierten Freigabeprozess im Lieferantenkontext prüfen und alle Informationen für den weiteren Freigabeprozess an den Softwarebetreiber übergeben. Zu den übergebenen Informationen gehören auch die Ergebnisse des eigenen Freigabeprozesses. (SYS.1.6.A13 S)

Der Softwarelieferant KANN...

* die zu liefernden Images bereits vor der finalen Lieferung mit dem Prüfstandard des Softwarebetreibers überprüfen. [^Liefer6] (SYS.1.6.A13 S)


Der Softwarebetreiber MUSS 

* die Informationen zum Prozess der Bereitstellung und Verteilung von Images an den Softwarelieferanten übergeben und sich mit diesen abstimmen. (SYS.1.6.A4 B)
* prüfen, dass die durch den Softwarelieferanten gelieferten Software-Artefakte nur einen Dienst je Container oder Verwaltungseinheit bereitstellen und geeignete Deployment-Anweisungen verfügbar sind. (SYS.1.6.A11 S)
* den sicheren Transport der Deployment-Artefakte, insb. Images, unter Berücksichtigung des Bausteins _CON.9 Informationsaustausch_ sicherstellen. (SYS.1.6.A12 S)
* die Freigabebedingungen in einem Freigabeprozess gemäß des Bausteines OPS.1.1.6 Software-Test und -Freigaben definieren. (SYS.1.6.A13 S)
* einen definierten Freigabeprozess für die Produktivsetzung der Anwendung / neuer Versionen etablieren. (SYS.1.6.A13 S)

Der Softwarebetreiber SOLL

* bei der Definition des Prozess zur Bereitstellung und Verteilung von Images folgendes berücksichtigen: (SYS.1.6.A4 B)
  * Auflistung erforderlicher Lizenzen
  * Bereitstellung von Deployment-Artefakten und Build-Spezifikationen
  * Prüfung auf Schwachstellen (statische und dynamische Tests), Schadsoftware, veraltete Komponenten, Build-Dependencies
  * Prüfsummen, Signaturen und Herkunft
  * Lifecycle und Patchmanagement von Images
  * Nutzung von vertrauenswürdigen Registries
* die Prüfung der durch den Softwarelieferanten bereitgestellten Artefakte und Deployment-Anweisungen automatisiert vornehmen und in seine CI/CD-Prozesse integrieren. (SYS.1.6.A11 S)
* allen Anforderungen entsprechenden Freigabebedingungen für die Inbetriebnahme von Images und Konfigurationen definieren und für den Softwarelieferanten bereitstellen. Hierbei sind die Freigabebedingungen des Plattformbetreibers zu berücksichtigen. (SYS.1.6.A13 S)

[^Liefer1]: Eine Software Bill of Materials (SBOM) führt alle Bestandteile einer Softwarekomponente in einem maschinenlesbaren Format auf und ermöglicht die statische Analyse der Software auf Schwachstellen und Lizenzen.

[^Liefer2]: Der Softwarebetreiber muss seine Prozess entsprechend anpassen können.
Die Nachvollziehbarkeit ist Grundlage für die Prüfung der gelieferten Software.

[^Liefer3]: Bereitstellung in einer Registry nach OCI-Registry-Format. 

[^Liefer4]: BSI TR-03183: Cyber-Resilienz-Anforderungen an Hersteller und Produkte – Teil 2: Software Bill of Materials (SBOM)

[^Liefer5]: Siehe dazu auch Baustein OPS.1.1.6 Software-Test und Freigaben

[^Liefer6]: Das ermöglicht dem Lieferanten, seine eigene Software qualitätszusichern und nicht vom Prüfergebnis des Betreibers "überrascht" zu werden. Das kann z.B. durch eine standardisierte Entwicklungsumgebung realisiert werden, die dem Lieferanten bereitgestellt wird.

[^Liefer7]: Gerade beim Test muss die strikte Trennung (Schutzbedarf und Mandant) umgesetzt werden, da hier Code ausgeführt wird.

