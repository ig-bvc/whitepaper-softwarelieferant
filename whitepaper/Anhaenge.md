# Anhänge

## Referenzen

|Kürzel|Beschreibung|
|--|------|
|SYS.1.6|Bundesamt für Sicherheit in der Informationstechnik (BSI). *IT-Grundschutz-Baustein SYS.1.6 Containerisierung*, 1. Februar 2022, online verfügbar unter [https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/IT-GS-Kompendium_Einzel_PDFs_2022/07_SYS_IT_Systeme/SYS_1_6_Containerisierung_Edition_2022.html](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/IT-GS-Kompendium_Einzel_PDFs_2022/07_SYS_IT_Systeme/SYS_1_6_Containerisierung_Edition_2022.html)|
|APP.4.4|Bundesamt für Sicherheit in der Informationstechnik (BSI). *IT-Grundschutz-Baustein APP.4.4 Kubernetes*, 1. Februar 2022, online verfügbar unter [https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/IT-GS-Kompendium_Einzel_PDFs_2022/06_APP_Anwendungen/APP_4_4_Kubernetes_Edition_2022.html](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/IT-GS-Kompendium_Einzel_PDFs_2022/06_APP_Anwendungen/APP_4_4_Kubernetes_Edition_2022.html)|
